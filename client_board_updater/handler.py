"""
Fetch Monday.com item updates and add them to a specified field.

Monday.com item updates cannot be searched, hence this lambda function is
responsible to copy all updates for an item to a specific "long text" field.

Since Monday.com has no REST API, this lambda function uses Monday.com's
GraphQL API which is sometimes unreliable.
"""

import logging
import os
import re
import sys

from .client import MondayClient
from .utils import get_secret

logger = logging.getLogger(__name__)

COLUMN_TO_UPDATE: str = "Updates"
UPDATE_SEPARATOR: str = "\n\n========== NEW UPDATE ========== \n\n"
URL_REGEX: str = r"https?:\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?"


def log_and_exit(message: str) -> None:
    """
    Create a new error log entry and exit the program with exit code 1.
    """

    logger.error(message)
    sys.exit(1)


def handle(req: str) -> str:
    """
    Update the specified column with the content of user updates on Monday.com.

    The function connects to Monday.com through its GraphQL API and fetches the
    updates for every item on a specific board. After the fetched updates are
    normalized, a dedicated column, identified by `COLUMN_TO_UPDATE` is updated
    with the fetched and normalized content.
    """

    url: str = os.getenv("MONDAY_API_URL", "https://api.monday.com/v2/")
    token: str = get_secret("monday-api-token")

    if not token:
        log_and_exit("missing MONDAY_API_TOKEN")

    try:
        board_id: int = int(os.getenv("MONDAY_BOARD_ID", ""))
    except ValueError:
        log_and_exit("invalid MONDAY_BOARD_ID")

    # ignore the type of token as it is already validated
    client = MondayClient(url, token)  # type: ignore
    update_column = client.find_column_by_title(board_id, COLUMN_TO_UPDATE)

    if not update_column:
        log_and_exit("cannot find column to update by title %s" % COLUMN_TO_UPDATE)

    for item in client.get_board_items_with_updates(board_id):
        if not item.updates:
            continue

        text = UPDATE_SEPARATOR.join([update.text_body for update in item.updates])

        # Remove URLs
        text = re.sub(URL_REGEX, "", text)

        # Replace multiple whitespaces with single whitespace
        text = re.sub(r"\s+", " ", text)

        client.change_column_value(
            board_id=board_id,
            item_id=item.id,
            column_id=update_column.id,  # type: ignore
            value={"text": text},
        )

    return "success"
