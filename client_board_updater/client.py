"""
Monday.com GraphQL client wrapper and related helper classes.
"""

import json
from dataclasses import dataclass
from typing import List, Optional

from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport


@dataclass
class ColumnsAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a board's columns.
    """

    id: str
    title: str


@dataclass
class UpdatesAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating updates belonging to items.
    """

    id: int
    text_body: str


@dataclass
class ItemsAPIResponse:
    """
    Response class represents the API response returned by Monday.com when
    querying or mutating a board's items.
    """

    id: int
    name: str
    updates: Optional[List[UpdatesAPIResponse]] = None


class MondayClient:
    """
    Client implementation to call Monday.com GraphQL API.
    """

    def __init__(self, url: str, token: str, timeout: int = 60) -> None:
        self.client = Client(
            transport=RequestsHTTPTransport(
                url=url,
                headers={"Authorization": token},
                timeout=timeout,
            ),
            execute_timeout=timeout,
        )

    def _execute(self, query: str, variables: Optional[dict] = None) -> dict:
        """
        Execute a query against monday.com API.
        """
        return self.client.execute(gql(query), variable_values=variables or dict())

    def get_board_items_with_updates(self, board_id: int) -> List[ItemsAPIResponse]:
        """
        Return the list of items created on a given board including the updates
        per item.
        """

        query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    items {
                        id
                        name
                        updates {
                            id
                            text_body
                        }
                    }
                }
            }
        """

        response = self._execute(query, variables={"board_ids": [int(board_id)]})
        items = response["boards"][0]["items"]

        return [
            ItemsAPIResponse(
                id=item["id"],
                name=item["name"],
                updates=[UpdatesAPIResponse(**update) for update in item["updates"]],
            )
            for item in items
        ]

    def get_columns(self, board_id: int) -> List[ColumnsAPIResponse]:
        """
        Return the list of columns and its ids related to a board.
        """

        query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    columns {
                        id
                        title
                    }
                }
            }
        """

        response = self._execute(query, variables={"board_ids": [int(board_id)]})
        columns = response["boards"][0]["columns"]
        return [ColumnsAPIResponse(**column) for column in columns]

    def find_column_by_title(
        self, board_id: int, title: str
    ) -> Optional[ColumnsAPIResponse]:
        """
        Return the column ID for board by title.

        Since the search is case-sensitive, in case the board has no column with
        the exact title, None will be returned.

        If multiple columns has the same title, the first found column_id will
        return.
        """

        matching_columns = filter(
            lambda column: column.title.lower() == title.lower(),
            self.get_columns(board_id),
        )

        return next(matching_columns, None)

    def change_column_value(
        self, board_id: int, item_id: int, column_id: str, value: dict
    ) -> ItemsAPIResponse:
        """
        Change a column's value on a board, identified by the item's and column's
        ID.
        """

        query = """
            mutation (
                    $board_id: Int!,
                    $item_id: Int,
                    $column_id: String!,
                    $value: JSON!
                ) {
                change_column_value(
                    board_id: $board_id,
                    item_id: $item_id,
                    column_id: $column_id,
                    value: $value
                ) {
                    id
                    name
                }
            }
        """

        response = self._execute(
            query,
            variables={
                "board_id": int(board_id),
                "item_id": int(item_id),
                "column_id": str(column_id),
                "value": json.dumps(value),
            },
        )

        return ItemsAPIResponse(**response["change_column_value"])
