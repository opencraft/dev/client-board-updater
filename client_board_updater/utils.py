from pathlib import Path


def get_secret(name: str) -> str:
    """
    Return a secret mapped as a file by OpenFAAS.

    Args:
        name: name of the secret

    Returns:
        Value secret value from the file.
    """

    return open(get_secret_path(name), "r").read().strip()


def get_secret_path(name: str) -> Path:
    """
    Return the given secrets' absolute path.

    Args:
        name: name of the secret

    Returns:
        Absolute path for the named secret.
    """

    return Path(f"/var/openfaas/secrets/{name}").absolute()

