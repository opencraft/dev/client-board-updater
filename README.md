# Client board updater

The client board updater script is intended to be an AWS Lambda function that is responsible for updating a hidden long field called "Updates" based on the updates provided for the board items.

## Development

_Disclaimer: In case you are not an administrator in Monday.com, you cannot generate a token for yourself._

To get started developing the handler, first you will need to Python 3.8 installed. To setup the project:

1. Create and activate a new virtualenv
1. Install requirements by running `pip install -r client_board_updater/requirements-test.txt`

No further setup or configuration is needed.

## Testing

To test the client locally, you will need to setup the project and set the appropriate environment variables. To setup the project, follow the instructions below:

1. Set Monday.com API Token by running `export MONDAY_API_TOKEN=<API_TOKEN>` where `<API_TOKEN>` is your token. In case you are not an administrator in Monday.com, you cannot generate a token for yourself.
1. Duplicate the Monday.com client board which contains the updates
1. Set the Monday.com board ID locally for the duplicated board by running `export MONDAY_BOARD_ID=<BOARD_ID>` where `<BOARD_ID>` is the ID of the duplicated board
1. Run `cd client_board_updater`
1. Run `handler.py` by executing `python handler.py`

## Deployment

The deployment is managed by the CI/CD pipeline and OpenFAAS. The function is deployed to production by default on merge. This script is not deployed to staging environment as we have no staging monday board either.

If you really want to check the changes on staging, you have to build and deploy to staging manually, using the same commands with corresponding enviornment variables that is in the CI/CD configuration (`.gitlab-ci.yml`).

## Setup for a new environment

1. In the [infrastructure repository](https://gitlab.com/opencraft/ops/infrastructure) activate the desired environment
1. Run the following command to create the `monday-api-token` secret.

   ```shell
   kubectl --namespace openfaas-fn create secret generic monday-api-token \
      --from-literal monday-api-token="<SECRET>"
   ```
1. Adjust the CI/CD variables as needed and/or setup CI "environments"
