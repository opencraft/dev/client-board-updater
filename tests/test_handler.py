# type: ignore

"""
Test AWS Lambda event handler.
"""

import re
from typing import List, Union
from unittest.mock import Mock, patch, call

import pytest

from client_board_updater.client import (
    ColumnsAPIResponse,
    ItemsAPIResponse,
    UpdatesAPIResponse,
)
from client_board_updater.handler import (
    COLUMN_TO_UPDATE,
    UPDATE_SEPARATOR,
    URL_REGEX,
    handle,
    log_and_exit,
)

def patch_secret(secret_value: Union[List[str], str]) -> callable:
    """
    Patch the `get_secret` function to return a dummy value.
    """
    def _patch_secret(func):
        @patch("client_board_updater.handler.get_secret")
        def wrapper(self, mock_get_secret, *args):
            # If we need to mock multiple secret values, do it as a list
            if isinstance(secret_value, list):
                mock_get_secret.side_effect = secret_value
            else:
                mock_get_secret.return_value = secret_value

            return func(self, *args)
        return wrapper
    return _patch_secret


class TestHelper:  # pylint: disable=too-few-public-methods
    """
    Test helper functions used by the handler to process the event.
    """

    @patch("client_board_updater.handler.logger")
    @patch("client_board_updater.handler.sys")
    def test_log_and_exit(self, mock_sys, mock_logger):  # pylint: disable=no-self-use
        """
        Test calling the log and exit function logs the given error message
        and terminates the process with exit code 1.
        """

        expected_message = "test message"
        log_and_exit(expected_message)

        mock_logger.error.assert_called_once_with(expected_message)
        mock_sys.exit.assert_called_once_with(1)


class TestHandler:
    """
    Test AWS Lambda event handler function.
    """

    event: dict = dict()
    context = None

    real_monday_api_url = "https://api.monday.com/v2/"

    monday_api_url = "https://example.com"
    monday_api_token = "secret token"
    monday_board_id = 12345

    expected_column_id = "col_id_123"
    expected_column_title = COLUMN_TO_UPDATE

    expected_update_1_id = 111
    expected_update_1_text_body = "update 1 text body"

    expected_update_2_id = 222
    expected_update_2_text_body = "update 2 text body"

    expected_item_id = 123
    expected_item_name = "item name"
    expected_item_updates = [
        UpdatesAPIResponse(
            id=expected_update_1_id, text_body=expected_update_1_text_body
        ),
        UpdatesAPIResponse(
            id=expected_update_2_id, text_body=expected_update_2_text_body
        ),
    ]


    @staticmethod
    def __normalize_text(updates: List[str]) -> str:
        """
        Normalize the text sent to the API.
        """

        text = UPDATE_SEPARATOR.join([update.text_body for update in updates])

        # Remove URLs
        text = re.sub(URL_REGEX, "", text)

        # Replace multiple whitespaces with single whitespace
        text = re.sub(r"\s+", " ", text)

        return text

    @patch("client_board_updater.handler.MondayClient")
    @patch("client_board_updater.handler.os")
    @patch_secret(secret_value="secret token")
    def test_handle(self, mock_os, mock_monday_client_class):
        """
        Test function invokation updating the items on the using its updates.
        """

        mock_os.getenv.side_effect = [
            self.monday_api_url,
            self.monday_board_id,
        ]

        mock_client = Mock()
        mock_monday_client_class.return_value = mock_client

        mock_client.find_column_by_title.return_value = ColumnsAPIResponse(
            id=self.expected_column_id, title=self.expected_column_title
        )

        mock_client.get_board_items_with_updates.return_value = [
            ItemsAPIResponse(
                id=self.expected_item_id,
                name=self.expected_item_name,
                updates=self.expected_item_updates,
            ),
        ]

        handle("{}")

        mock_monday_client_class.assert_called_once_with(
            self.monday_api_url,
            self.monday_api_token,
        )
        mock_client.find_column_by_title.assert_called_once_with(
            self.monday_board_id, COLUMN_TO_UPDATE
        )
        mock_client.get_board_items_with_updates.assert_called_once_with(
            self.monday_board_id
        )
        mock_client.change_column_value.assert_called_once_with(
            board_id=self.monday_board_id,
            item_id=self.expected_item_id,
            column_id=self.expected_column_id,
            value={
                "text": self.__normalize_text(self.expected_item_updates),
            },
        )

    @patch("client_board_updater.handler.os")
    @patch("client_board_updater.handler.logger")
    @patch_secret(secret_value="")
    def test_handle_no_token(self, mock_logger, mock_os):
        """
        Test function logging an error stating the API token is not configured.
        """

        mock_os.getenv.side_effect = [self.monday_api_url, None]

        with pytest.raises(SystemExit):
            handle("{}")

        mock_os.getenv.assert_has_calls(
            [call("MONDAY_API_URL", self.real_monday_api_url)]
        )
        mock_logger.error.assert_called_once_with("missing MONDAY_API_TOKEN")

    @patch("client_board_updater.handler.os")
    @patch("client_board_updater.handler.logger")
    @patch_secret(secret_value="secret token")
    def test_handle_no_board_id(self, mock_logger, mock_os):
        """
        Test the function logging an error stating the board id is invalid.
        """

        mock_os.getenv.side_effect = [
            self.monday_api_url,
            self.monday_api_token,
            "invalid board id",
        ]

        with pytest.raises(SystemExit):
            handle("{}")

        mock_os.getenv.assert_has_calls(
            [
                call("MONDAY_API_URL", self.real_monday_api_url),
                call("MONDAY_BOARD_ID", ""),
            ]
        )
        mock_logger.error.assert_called_once_with("invalid MONDAY_BOARD_ID")

    @patch("client_board_updater.handler.MondayClient")
    @patch("client_board_updater.handler.os")
    @patch_secret(secret_value="secret token")
    def test_handle_no_items(self, mock_os, mock_monday_client_class):
        """
        Test the function executes properly, even if the board has no items.

        This is a test scenario that is "impossible" in the real life as we have
        clients.
        """

        mock_os.getenv.side_effect = [
            self.monday_api_url,
            self.monday_board_id,
        ]

        mock_client = Mock()
        mock_monday_client_class.return_value = mock_client

        mock_client.find_column_by_title.return_value = ColumnsAPIResponse(
            id=self.expected_column_id, title=self.expected_column_title
        )

        mock_client.get_board_items_with_updates.return_value = []

        handle("{}")

        mock_monday_client_class.assert_called_once_with(
            self.monday_api_url,
            self.monday_api_token,
        )
        mock_client.find_column_by_title.assert_called_once_with(
            self.monday_board_id, COLUMN_TO_UPDATE
        )
        mock_client.get_board_items_with_updates.assert_called_once_with(
            self.monday_board_id
        )
        assert mock_client.change_column_value.called is False

    @patch("client_board_updater.handler.MondayClient")
    @patch("client_board_updater.handler.os")
    @patch_secret(secret_value="secret token")
    def test_handle_no_updates(self, mock_os, mock_monday_client_class):
        """
        Test the function executes properly for items which has no updates.
        """

        mock_os.getenv.side_effect = [
            self.monday_api_url,
            self.monday_board_id,
        ]

        mock_client = Mock()
        mock_monday_client_class.return_value = mock_client

        mock_client.find_column_by_title.return_value = ColumnsAPIResponse(
            id=self.expected_column_id, title=self.expected_column_title
        )

        mock_client.get_board_items_with_updates.return_value = [
            ItemsAPIResponse(
                id=self.expected_item_id, name=self.expected_item_name, updates=[]
            )
        ]

        handle("{}")

        mock_monday_client_class.assert_called_once_with(
            self.monday_api_url,
            self.monday_api_token,
        )
        mock_client.find_column_by_title.assert_called_once_with(
            self.monday_board_id, COLUMN_TO_UPDATE
        )
        mock_client.get_board_items_with_updates.assert_called_once_with(
            self.monday_board_id
        )
        assert mock_client.change_column_value.called is False
