# type: ignore

"""
Test Monday.com API client.
"""


import json
from unittest.mock import Mock, call, patch

import pytest

from client_board_updater.client import (
    MondayClient,
    ColumnsAPIResponse,
    UpdatesAPIResponse,
    ItemsAPIResponse,
)


@pytest.fixture()
@patch("client_board_updater.client.Client")
def monday_client(_) -> MondayClient:
    """
    Fixture returning a patched MondayClient to ensure we don't call Monday.com.

    We cannot use the monday_client defined in clients.monday since that client
    is initialized with an unmocked GraphQL client.
    """

    client = MondayClient(
        url="https://example.com",
        token="secret token",
    )

    client._execute = Mock()  # pylint: disable=protected-access

    return client


# pylint: disable=redefined-outer-name,protected-access,no-self-use
class TestMondayClient:
    """
    Test MondayClient functionalities.

    Test that we are able to call all the required operations with the expected
    queries, mutations and variables. The client is responsible for the communication
    with monday.com, hence it should be the single source of truth for retrieving data
    and the only component which can interact with monday.com.
    """

    expected_board_id = 123
    expected_board_name = "client test board"
    expected_column_id = "testcol"
    expected_column_text = "Test Column Value"
    expected_column_title = "Test Column"
    expected_item_id = 123321
    expected_item_name = "Item 1"
    expected_update_id = 123456
    expected_update_text_body = "update text body"

    def test_get_columns(self, monday_client: MondayClient):
        """
        Test get columns of a board.

        Test getting columns with the right query and variables and returns the
        expected dataclass.
        """

        expected_query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    columns {
                        id
                        title
                    }
                }
            }
        """

        expected_variables = {"board_ids": [self.expected_board_id]}

        monday_client._execute.return_value = {
            "boards": [
                {
                    "columns": [
                        {
                            "id": self.expected_column_id,
                            "title": self.expected_column_title,
                        },
                    ],
                },
            ],
        }

        result = monday_client.get_columns(self.expected_board_id)
        column = result[0]

        assert isinstance(result, list)
        assert isinstance(column, ColumnsAPIResponse)
        assert column.id == self.expected_column_id
        assert column.title == self.expected_column_title

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_get_board_items_with_updates(self, monday_client: MondayClient):
        """
        Test get items for a board.

        Test getting items with the right query and variables and returns the
        expected dataclass.
        """

        expected_query = """
            query ($board_ids: [Int]) {
                boards(ids: $board_ids) {
                    items {
                        id
                        name
                        updates {
                            id
                            text_body
                        }
                    }
                }
            }
        """

        expected_variables = {"board_ids": [self.expected_board_id]}

        monday_client._execute.return_value = {
            "boards": [
                {
                    "items": [
                        {
                            "id": self.expected_item_id,
                            "name": self.expected_item_name,
                            "updates": [
                                {
                                    "id": self.expected_update_id,
                                    "text_body": self.expected_update_text_body,
                                },
                            ],
                        },
                    ],
                },
            ],
        }

        result = monday_client.get_board_items_with_updates(self.expected_board_id)
        item = result[0]

        assert isinstance(result, list)
        assert isinstance(item, ItemsAPIResponse)
        assert item.id == self.expected_item_id
        assert item.name == self.expected_item_name
        assert item.updates == [
            UpdatesAPIResponse(
                id=self.expected_update_id, text_body=self.expected_update_text_body
            ),
        ]

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )

    def test_find_column_by_title(self, monday_client: MondayClient):
        """
        Test find a column ID by the column's title.

        Test getting column ID by its column title with the right query and
        variables and returns the expected dataclass.
        """

        monday_client.get_columns = Mock()
        monday_client.get_columns.return_value = [
            ColumnsAPIResponse(
                id="other_column",
                title="other title",
            ),
            ColumnsAPIResponse(
                id=self.expected_column_id,
                title=self.expected_column_title,
            ),
            ColumnsAPIResponse(
                id="another_column",
                title="another title",
            ),
        ]

        result = monday_client.find_column_by_title(
            self.expected_board_id, self.expected_column_title
        )

        assert isinstance(result, ColumnsAPIResponse)
        assert result.id == self.expected_column_id
        assert result.title == self.expected_column_title

        monday_client.get_columns.assert_called_once_with(self.expected_board_id)

    def test_change_column_value(self, monday_client: MondayClient):
        """
        Test change the value of a column.

        Test changing column values called with the right query and variables
        and returns the expected dataclass.
        """

        expected_value = {"key": "value"}

        expected_query = """
            mutation (
                    $board_id: Int!,
                    $item_id: Int,
                    $column_id: String!,
                    $value: JSON!
                ) {
                change_column_value(
                    board_id: $board_id,
                    item_id: $item_id,
                    column_id: $column_id,
                    value: $value
                ) {
                    id
                    name
                }
            }
        """

        expected_variables = {
            "board_id": self.expected_board_id,
            "item_id": self.expected_item_id,
            "column_id": self.expected_column_id,
            "value": json.dumps(expected_value),
        }

        monday_client._execute.return_value = {
            "change_column_value": {
                "id": self.expected_item_id,
                "name": self.expected_item_name,
            },
        }

        result = monday_client.change_column_value(
            self.expected_board_id,
            self.expected_item_id,
            self.expected_column_id,
            expected_value,
        )

        assert isinstance(result, ItemsAPIResponse)
        assert result.id == self.expected_item_id
        assert result.name == self.expected_item_name

        monday_client._execute.assert_called_once_with(
            expected_query, variables=expected_variables
        )
